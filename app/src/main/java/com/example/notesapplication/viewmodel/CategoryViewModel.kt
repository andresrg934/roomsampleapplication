package com.example.notesapplication.viewmodel

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.notesapplication.data.CategoriesRepository
import com.example.notesapplication.data.models.Category
import com.example.notesapplication.data.models.CategoryWithNotes
import com.example.notesapplication.database.NoteDatabase

class CategoryViewModel(private val repository: CategoriesRepository): ViewModel() {

    private var allCategoriesWithNotes: LiveData<List<CategoryWithNotes>>? = null

    init {
        allCategoriesWithNotes = repository.getCategoriesWithNotes()
    }

    fun addCategory(category: Category) {
        repository.addCategory(category)
    }

    fun updateCategory(category: Category) {
        repository.updateCategory(category)
    }

    fun deleteCategory(category: Category) {
        repository.deleteCategory(category)
    }

    suspend fun getCategoryById(idNote: Int) = repository.getCategoryById(idNote)

    fun getAllCategoriesWithNotes() = allCategoriesWithNotes

    internal class Factory(private val categoryRepository: CategoriesRepository) : ViewModelProvider.Factory {
        companion object {
            var newInstance = { repository: CategoriesRepository ->
                Factory(repository)
            }
        }

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return CategoryViewModel(categoryRepository) as? T
                ?: throw IllegalArgumentException("This factory can only create CategoryViewModel instances")
        }
    }
}

fun getCategoryViewModel(
    fragmentScope: Fragment
): CategoryViewModel {
    val dao = NoteDatabase(fragmentScope.requireContext()).getCategoryDao()
    val repository = CategoriesRepository(dao)
    val factory = CategoryViewModel.Factory.newInstance(repository)
    return ViewModelProvider(fragmentScope, factory).get(CategoryViewModel::class.java)
}