package com.example.notesapplication.viewmodel

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.notesapplication.data.NotesRepository
import com.example.notesapplication.data.models.Note
import com.example.notesapplication.database.NoteDatabase

class NotesViewModel(private val repository: NotesRepository) : ViewModel() {

    private var allNotes: LiveData<List<Note>>? = null
    private var notesByCategory: LiveData<List<Note>>? = null

    init {
        allNotes = repository.getValidNotes()
        notesByCategory = repository.getNotesByCategory()
    }

    fun addNotes(note: Note) {
        repository.addNote(note)
    }

    fun updateNotes(note: Note) {
        repository.updateNote(note)
    }

    fun deleteNote(note: Note) {
        repository.deleteNote(note)
    }

    suspend fun getNoteById(idNote: Int) = repository.getNoteById(idNote)

    fun getAllNotes() = allNotes

    fun getNotesByCategory() = notesByCategory

    internal class Factory(
        private val notesRepository: NotesRepository
    ) : ViewModelProvider.Factory {
        companion object {
            var newInstance = { repository: NotesRepository ->
                Factory(repository)
            }
        }

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return NotesViewModel(notesRepository) as? T
                ?: throw IllegalArgumentException("This factory can only create OrdersListViewModel instances")
        }
    }
}

fun getNotesViewModel(
    idCategory: Int,
    fragmentScope: Fragment
): NotesViewModel {
    val dao = NoteDatabase(fragmentScope.requireContext()).getNoteDao()
    val repository = NotesRepository(idCategory, dao)
    val factory = NotesViewModel.Factory.newInstance(repository)
    return ViewModelProvider(fragmentScope, factory).get(NotesViewModel::class.java)
}