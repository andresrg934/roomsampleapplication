package com.example.notesapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*

private const val CATEGORY_LIST_TAG = "CATEGORY_LIST"
private const val NOTE_LIST_TAG = "NOTE_LIST"
private const val NOTE_DETAILS_TAG = "NOTE_DETAILS"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = "Notes"
        loadCategoryList()
    }

    override fun onAttachFragment(fragment: Fragment) {
        super.onAttachFragment(fragment)
        when (fragment) {
            is CategoryListFragment -> {
                fragment.configure(::categoryItemClickTrigger)
            }
            is NoteListFragment -> {
                fragment.configure(::noteItemClickTrigger)
            }
            is NoteFragment -> {
                fragment.configure(::onBackPressed)
            }
        }
    }

    private fun loadCategoryList() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment_container, CategoryListFragment(), CATEGORY_LIST_TAG)
            commit()
        }
    }

    private fun loadNoteList(idCategory: Int) {
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val fragment = NoteListFragment().withArguments(idCategory)
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment_container, fragment, NOTE_LIST_TAG)
            addToBackStack(NOTE_LIST_TAG)
            commit()
        }
    }

    private fun loadNoteDetails(idNote: Int, idCategory: Int) {
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val fragment = NoteFragment().withArguments(idNote, idCategory)
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment_container, fragment, NOTE_DETAILS_TAG)
            addToBackStack(NOTE_DETAILS_TAG)
            commit()
        }
    }

    private fun noteItemClickTrigger(noteId: Int, categoryId: Int) {
        loadNoteDetails(noteId, categoryId)
    }

    private fun categoryItemClickTrigger(id: Int) {
        loadNoteList(id)
    }

    override fun onBackPressed() {
        if (!supportFragmentManager.popBackStackImmediate()) {
            super.onBackPressed()
        }
    }
}