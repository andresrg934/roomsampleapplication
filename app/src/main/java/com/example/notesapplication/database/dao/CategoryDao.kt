package com.example.notesapplication.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.notesapplication.data.models.Category
import com.example.notesapplication.data.models.CategoryWithNotes
import com.example.notesapplication.data.models.Note

@Dao
interface CategoryDao {

    @Insert
    suspend fun addCategory(category: Category)

    @Query("SELECT * FROM category WHERE id != -1")
    fun getAllCategories(): LiveData<List<Category>>

    @Query("SELECT * FROM category WHERE id = :idCategory")
    suspend fun getCategoryById(idCategory: Int): Category?

    @Transaction
    @Query("SELECT * FROM Category")
    fun getCategoriesWithNotes(): LiveData<List<CategoryWithNotes>>

    @Update
    suspend fun updateCategory(category: Category)

    @Delete
    suspend fun deleteCategory(category: Category)
}