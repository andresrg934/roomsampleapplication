package com.example.notesapplication.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.notesapplication.data.models.Note

@Dao
interface NoteDao {

    @Insert
    suspend fun addNote(note: Note)

    @Query("SELECT * FROM note WHERE id != -1")
    fun getAllValidNotes(): LiveData<List<Note>>

    @Query("SELECT * FROM note WHERE category_id = :categoryId")
    fun getNotesByCategoryId(categoryId: Int): LiveData<List<Note>>

    @Query("SELECT * FROM note WHERE id = :idNote")
    suspend fun getNoteById(idNote: Int): Note?

    @Update
    suspend fun updateNote(note: Note)

    @Delete
    suspend fun deleteNote(note: Note)
}