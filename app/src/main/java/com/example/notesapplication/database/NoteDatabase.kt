package com.example.notesapplication.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.notesapplication.data.models.Category
import com.example.notesapplication.database.dao.NoteDao
import com.example.notesapplication.data.models.Note
import com.example.notesapplication.database.dao.CategoryDao
import com.example.notesapplication.utils.Converters

private const val DATABASE_NAME = "notedatabase"
private const val DATABASE_VERSION = 3

@Database(
    entities = [Note::class, Category::class],
    version = DATABASE_VERSION
)
@TypeConverters(Converters::class)
abstract class NoteDatabase : RoomDatabase() {

    abstract fun getNoteDao(): NoteDao
    abstract fun getCategoryDao(): CategoryDao

    companion object {
        private var instance: NoteDatabase? = null

        operator fun invoke(context: Context) =
            instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                NoteDatabase::class.java,
                DATABASE_NAME
            ).build()
    }
}