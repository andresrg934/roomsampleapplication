package com.example.notesapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.notesapplication.data.adapter.notes.NotesAdapter
import com.example.notesapplication.data.models.Note
import com.example.notesapplication.utils.BaseFragment
import com.example.notesapplication.viewmodel.getNotesViewModel
import kotlinx.android.synthetic.main.fragment_note_list.*

private const val CATEGORY_ID_KEY = "key:CATEGORY_ID"

class NoteListFragment : BaseFragment() {

    fun withArguments(
        categoryId: Int
    ) = apply {
        arguments = Bundle().apply {
            putInt(CATEGORY_ID_KEY, categoryId)
        }
    }

    private val idCategory by lazy {
        arguments?.getInt(CATEGORY_ID_KEY)
            ?: throw IllegalArgumentException("There should be a category id")
    }

    private val viewModel by lazy {
        getNotesViewModel(idCategory, this)
    }

    private lateinit var itemClickObserver: (Int, Int) -> Unit

    private val notesAdapter by lazy {
        NotesAdapter(
            itemClickObserver,
            ::longPressDelete
        )
    }

    fun configure(itemClickObserver: (Int, Int) -> Unit) {
        this.itemClickObserver = itemClickObserver
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_note_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecycler()
        initFabClick()

        viewModel.getNotesByCategory()?.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                tv_no_notes.visibility = View.GONE
            } else {
                tv_no_notes.visibility = View.VISIBLE
            }
            setData(it)
        })
    }

    private fun setupRecycler() {
        notes_recycler.run {
            adapter = notesAdapter
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        }
    }

    private fun setData(items: List<Note>) {
        notesAdapter.submitList(items.toMutableList())
    }

    private fun initFabClick() {
        newNoteButton.setOnClickListener {
            itemClickObserver(-1, idCategory)
        }
    }

    private fun longPressDelete(note: Note) {
        viewModel.deleteNote(note)
        Toast.makeText(requireContext(), "Note ${note.title} deleted", Toast.LENGTH_LONG).show()
    }
}
