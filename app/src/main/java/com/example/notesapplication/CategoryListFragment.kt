package com.example.notesapplication

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.DialogCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.notesapplication.data.adapter.category.CategoriesAdapter
import com.example.notesapplication.data.models.Category
import com.example.notesapplication.data.models.CategoryWithNotes
import com.example.notesapplication.utils.BaseFragment
import com.example.notesapplication.viewmodel.getCategoryViewModel
import kotlinx.android.synthetic.main.fragment_category_list.*


class CategoryListFragment : BaseFragment() {

    private val viewModel by lazy {
        getCategoryViewModel(this)
    }

    private lateinit var itemClickObserver: (Int) -> Unit

    private val categoriesAdapter by lazy {
        CategoriesAdapter(
            itemClickObserver,
            ::editButtonPressed
        )
    }

    fun configure(itemClickObserver: (Int) -> Unit) {
        this.itemClickObserver = itemClickObserver
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_category_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecycler()
        initFabClick()

        viewModel.getAllCategoriesWithNotes()?.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                tv_no_categories.visibility = View.GONE
            } else {
                tv_no_categories.visibility = View.VISIBLE
            }
            setData(it)
        })
    }

    private fun setupRecycler() {
        categories_recycler.run {
            adapter = categoriesAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            setupSwipeToDelete()
        }
    }

    private fun initFabClick() {
        newCategoryButton.setOnClickListener {
            requireContext().showCategoryDialog()
        }
    }

    private fun setData(items: List<CategoryWithNotes>) {
        categoriesAdapter.submitList(items.toMutableList())
    }

    private fun Context.showCategoryDialog(category: Category? = null) {
        var defaultTitle = "New Category"
        var defaultContent = ""

        category?.run {
            defaultTitle = "Update Category"
            defaultContent = title
        }

        val view = layoutInflater.inflate(R.layout.new_category_dialog, null)

        val input: EditText = view.findViewById(R.id.etCategoryTitle)
        input.setText(defaultContent)

        val positiveButton: Button = view.findViewById(R.id.saveButton)
        val negativeButton: Button = view.findViewById(R.id.cancelButton)

        val dialog = AlertDialog.Builder(this).apply {
            setTitle(defaultTitle)
            setView(view)
        }.show()

        positiveButton.setOnClickListener {
            val categoryTitle = input.text.toString()
            if (categoryTitle.isNotBlank()) {
                val newCategory = Category(title = categoryTitle)
                category?.run {
                    newCategory.id = this.id
                    viewModel.updateCategory(newCategory)
                } ?: viewModel.addCategory(newCategory)
            }
            dialog.dismiss()
        }
        negativeButton.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun RecyclerView.setupSwipeToDelete() {
        ItemTouchHelper(object: ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ) = false

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val category = categoriesAdapter.getCategoryAtPosition(viewHolder.adapterPosition)
                viewModel.deleteCategory(category)
                Toast.makeText(requireContext(), "${category.title} category and Notes deleted", Toast.LENGTH_LONG).show()
            }

        }).attachToRecyclerView(this)
    }

    private fun editButtonPressed(category: Category) {
        requireContext().showCategoryDialog(category)
    }
}
