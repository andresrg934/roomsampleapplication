package com.example.notesapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.notesapplication.data.models.Note
import com.example.notesapplication.utils.BaseFragment
import com.example.notesapplication.viewmodel.getNotesViewModel
import kotlinx.android.synthetic.main.fragment_note.*
import kotlinx.coroutines.runBlocking

private const val NOTE_ID_KEY = "key:NOTE_ID"
private const val CATEGORY_ID_KEY = "key:CATEGORY_ID"

class NoteFragment : BaseFragment() {

    private lateinit var onBack: () -> Unit

    fun withArguments(
        noteId: Int,
        categoryId: Int
    ) = apply {
        arguments = Bundle().apply {
            putInt(NOTE_ID_KEY, noteId)
            putInt(CATEGORY_ID_KEY, categoryId)
        }
    }

    private val viewModel by lazy {
        getNotesViewModel(idCategory, this)
    }

    private val idNote by lazy {
        arguments?.getInt(NOTE_ID_KEY) ?: -1
    }

    private val idCategory by lazy {
        arguments?.getInt(CATEGORY_ID_KEY)
            ?: throw IllegalArgumentException("There should be a category id")
    }

    fun configure(onBack: () -> Unit) {
        this.onBack = onBack
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_note, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initContent()

        saveNoteButton.setOnClickListener {
            val title = text_note_title.text.toString()
            val content = text_note_content.text.toString()

            if (title.isEmpty()) {
                text_note_title.error = "Title is required"
                text_note_title.requestFocus()
                return@setOnClickListener
            }

            if (content.isEmpty()) {
                text_note_content.error = "Content is required"
                text_note_content.requestFocus()
                return@setOnClickListener
            }

            Note(
                title = title,
                note = content,
                idCategory = idCategory
            ).saveData()

            onBack()
        }
    }

    private fun Note.saveData() {
        if (idNote > 0) {
            id = idNote
            viewModel.updateNotes(this)
            showToast("Note $id updated!")
        } else {
            viewModel.addNotes(this)
            showToast("New note added!")
        }
    }

    private fun initContent() {
        if (idNote > 0) {
            runBlocking {
                viewModel.getNoteById(idNote)?.run {
                    text_note_title.setText(title)
                    text_note_content.setText(note)
                }
            }
        }
    }

    private fun showToast(content: String) {
        Toast.makeText(requireContext(), content, Toast.LENGTH_LONG).show()
    }
}