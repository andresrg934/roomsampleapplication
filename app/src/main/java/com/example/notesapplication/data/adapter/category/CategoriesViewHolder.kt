package com.example.notesapplication.data.adapter.category

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.notesapplication.R
import com.example.notesapplication.data.models.Category
import com.example.notesapplication.data.models.CategoryWithNotes
import com.example.notesapplication.data.models.Note

class CategoriesViewHolder(
    private val view: View
): RecyclerView.ViewHolder(view) {

    fun bind(item: CategoryWithNotes, editButtonPressed: (Category) -> Unit) {
        val title: TextView = view.findViewById(R.id.category_title)
        val content: TextView = view.findViewById(R.id.notes_count)
        val editButton: ImageView = view.findViewById(R.id.editButton)
        editButton.setOnClickListener {
            editButtonPressed(item.category)
        }

        title.text = item.category.title
        content.text = "${item.notes.count()}"
    }
}