package com.example.notesapplication.data.adapter.category

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.notesapplication.R
import com.example.notesapplication.data.models.Category
import com.example.notesapplication.data.models.CategoryWithNotes
import com.example.notesapplication.data.models.Note

class CategoriesAdapter(
    val onItemClick: (Int) -> Unit,
    val editButtonPressed: (Category) -> Unit
) : ListAdapter<CategoryWithNotes, CategoriesViewHolder>(CategoriesDiffCallback) {

    private var items = listOf<CategoryWithNotes>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CategoriesViewHolder(inflater.inflate(R.layout.category_item, parent, false))
    }

    override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) {
        holder.bind(items[position], editButtonPressed)
        holder.itemView.setOnClickListener {
            onItemClick(items[position].category.id)
        }
    }

    override fun submitList(list: MutableList<CategoryWithNotes>?) {
        items = list ?: emptyList()
        super.submitList(list)
    }

    fun getCategoryAtPosition(position: Int) = items[position].category
}

object CategoriesDiffCallback : DiffUtil.ItemCallback<CategoryWithNotes>() {
    override fun areItemsTheSame(oldItem: CategoryWithNotes, newItem: CategoryWithNotes) = oldItem.category.id == newItem.category.id

    override fun areContentsTheSame(oldItem: CategoryWithNotes, newItem: CategoryWithNotes) = oldItem == newItem
}
