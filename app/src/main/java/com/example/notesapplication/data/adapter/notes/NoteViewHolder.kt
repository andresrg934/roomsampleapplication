package com.example.notesapplication.data.adapter.notes

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.notesapplication.R
import com.example.notesapplication.data.models.Note

class NoteViewHolder(
    private val view: View
): RecyclerView.ViewHolder(view) {

    fun bind(item: Note) {
        val title: TextView = view.findViewById(R.id.note_title)
        val content: TextView = view.findViewById(R.id.note_content)

        title.text = item.title
        content.text = item.note
    }
}