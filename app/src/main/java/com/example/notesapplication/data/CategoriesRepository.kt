package com.example.notesapplication.data

import androidx.lifecycle.LiveData
import com.example.notesapplication.data.models.Category
import com.example.notesapplication.data.models.CategoryWithNotes
import com.example.notesapplication.database.dao.CategoryDao
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class CategoriesRepository(private val dao: CategoryDao) : CoroutineScope {

    private val allCategoriesWithNotes: LiveData<List<CategoryWithNotes>> = dao.getCategoriesWithNotes()
    private val job: Job
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    init {
        job = SupervisorJob()
    }

    fun addCategory(category: Category) {
        launch(Dispatchers.IO) {
            dao.addCategory(category)
        }
    }

    fun updateCategory(category: Category) {
        launch(Dispatchers.IO) {
            dao.updateCategory(category)
        }
    }

    fun deleteCategory(category: Category) {
        launch(Dispatchers.IO) {
            dao.deleteCategory(category)
        }
    }

    fun getCategoriesWithNotes() = allCategoriesWithNotes

    suspend fun getCategoryById(idCategory: Int) = dao.getCategoryById(idCategory)

    companion object {
        private var instance: CategoriesRepository? = null

        operator fun invoke(dao: CategoryDao) =
            getInstance(
                dao
            )

        private fun getInstance(quoteDao: CategoryDao) =
            instance
                ?: synchronized(this) {
                    instance
                        ?: CategoriesRepository(quoteDao)
                            .also {
                                instance = it
                            }
                }
    }
}