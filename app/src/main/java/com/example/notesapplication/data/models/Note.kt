package com.example.notesapplication.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import java.util.*

@Entity(
    foreignKeys =
    [
        ForeignKey(
            entity = Category::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("category_id"),
            onDelete = CASCADE
        )
    ]
)
data class Note(
    @ColumnInfo(name = "note_title") val title: String,
    val note: String,
    @ColumnInfo(name = "category_id") val idCategory: Int,
    @ColumnInfo(name = "creation_date") val creationDate: Date = Date()
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}