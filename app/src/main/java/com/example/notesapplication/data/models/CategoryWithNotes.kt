package com.example.notesapplication.data.models

import androidx.room.*
import java.util.*

data class CategoryWithNotes(
    @Embedded val category: Category,
    @Relation(
        parentColumn = "id",
        entityColumn = "category_id"
    )
    val notes: List<Note>
)