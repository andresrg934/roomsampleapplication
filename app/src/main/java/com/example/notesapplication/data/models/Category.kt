package com.example.notesapplication.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Category(
    @ColumnInfo(name = "category_title") val title: String,
    @ColumnInfo(name = "creation_date") val creationDate: Date = Date()
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}