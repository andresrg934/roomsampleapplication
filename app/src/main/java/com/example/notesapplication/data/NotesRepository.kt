package com.example.notesapplication.data

import androidx.lifecycle.LiveData
import com.example.notesapplication.database.dao.NoteDao
import com.example.notesapplication.data.models.Note
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class NotesRepository(
    idCategory: Int,
    private val dao: NoteDao
) : CoroutineScope {

    private val allNotes: LiveData<List<Note>> = dao.getAllValidNotes()
    private val notesByCategory: LiveData<List<Note>> = dao.getNotesByCategoryId(idCategory)
    private val job: Job
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    init {
        job = SupervisorJob()
    }

    fun addNote(note: Note) {
        launch(Dispatchers.IO) {
            dao.addNote(note)
        }
    }

    fun updateNote(note: Note) {
        launch(Dispatchers.IO) {
            dao.updateNote(note)
        }
    }

    fun deleteNote(note: Note) {
        launch(Dispatchers.IO) {
            dao.deleteNote(note)
        }
    }

    fun getValidNotes() = allNotes

    fun getNotesByCategory() = notesByCategory

    suspend fun getNoteById(idNote: Int) = dao.getNoteById(idNote)

    companion object {
        private var instance: NotesRepository? = null

        operator fun invoke(idCategory: Int, dao: NoteDao) =
            getInstance(
                idCategory,
                dao
            )

        private fun getInstance(idCategory: Int, quoteDao: NoteDao) =
            instance
                ?: synchronized(this) {
                    instance
                        ?: NotesRepository(idCategory, quoteDao)
                            .also {
                                instance = it
                            }
                }
    }
}