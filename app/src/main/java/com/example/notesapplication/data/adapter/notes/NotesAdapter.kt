package com.example.notesapplication.data.adapter.notes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.notesapplication.R
import com.example.notesapplication.data.models.Note

class NotesAdapter(
    val onItemClick: (Int, Int) -> Unit,
    val longPressDelete: (Note) -> Unit
) : ListAdapter<Note, NoteViewHolder>(
    NotesDiffCallback
) {

    private var items = listOf<Note>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return NoteViewHolder(
            inflater.inflate(
                R.layout.note_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.bind(items[position])
        holder.itemView.setOnClickListener {
            onItemClick(items[position].id, items[position].idCategory)
        }
        holder.itemView.setOnLongClickListener {
            longPressDelete(items[position])
            true
        }
    }

    override fun submitList(list: MutableList<Note>?) {
        items = list ?: emptyList()
        super.submitList(list)
    }

}

object NotesDiffCallback : DiffUtil.ItemCallback<Note>() {
    override fun areItemsTheSame(oldItem: Note, newItem: Note) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Note, newItem: Note) = oldItem == newItem
}
